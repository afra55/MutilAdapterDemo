package com.afra.formutleview.sugar;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.afra.formutleview.R;
import com.afra.formutleview.base.BaseRecyclerAdapter;
import com.afra.formutleview.base.EventBusHelper;
import com.afra.formutleview.base.GlideApp;
import com.afra.formutleview.base.ViewSugar;
import com.afra.formutleview.bean.BaseBean;
import com.afra.formutleview.bean.NeneGridBean;
import com.afra.formutleview.bean.NeneGridItemBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yangshuai on 2018/7/13.
 * {link http://afra55.github.io}
 */

public class NineGridItemSugar extends ViewSugar{

    public static ViewSugar getInstance(Context context, ViewGroup viewGroup) {
        return new NineGridItemSugar(context, viewGroup);
    }

    private ImageView imageView;

    public NineGridItemSugar(Context context, ViewGroup parent) {
        super(context, parent);
        imageView = findView(R.id.image_view);
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_for_nine_grid_item;
    }

    @Override
    public <T extends BaseBean> void bind(T obj) {

        if (obj instanceof NeneGridItemBean) {
            NeneGridItemBean itemBean = (NeneGridItemBean) obj;
            String imagePath = itemBean.getImagePath();
            if (TextUtils.isEmpty(imagePath)) {
                GlideApp.with(context)
                        .load("http://pic1.win4000.com/wallpaper/2018-04-27/5ae28785b2020_270_185.jpg")
                        .into(imageView);
            } else {
                GlideApp.with(context)
                        .load(imagePath)
                        .into(imageView);
            }
        }

    }
}
