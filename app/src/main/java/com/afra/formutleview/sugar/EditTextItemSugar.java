package com.afra.formutleview.sugar;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.afra.formutleview.R;
import com.afra.formutleview.base.ViewSugar;
import com.afra.formutleview.bean.BaseBean;
import com.afra.formutleview.bean.EditTextBean;
import com.afra.formutleview.bean.TitleBean;

/**
 * Created by yangshuai on 2018/7/13.
 * {link http://afra55.github.io}
 */

public class EditTextItemSugar extends ViewSugar{

    public static ViewSugar getInstance(Context context, ViewGroup viewGroup) {
        return new EditTextItemSugar(context, viewGroup);
    }

    private EditText editText;
    private TextView title;

    public EditTextItemSugar(Context context, ViewGroup parent) {
        super(context, parent);
        title = findView(R.id.title);
        editText = findView(R.id.edit_text);
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_for_edittext;
    }

    @Override
    public <T extends BaseBean> void bind(T obj) {

        if (obj instanceof EditTextBean) {
            final EditTextBean bean = (EditTextBean) obj;
            title.setText(bean.getTitle());
            String content = bean.getContent();
            if (!TextUtils.isEmpty(content)) {
                editText.setText(content);
            }
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    // 这样数据就存入 最外层data 中　
                    bean.setContent(editable.toString());
                }
            });
        }

    }
}
