package com.afra.formutleview.sugar;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.afra.formutleview.R;
import com.afra.formutleview.base.ViewSugar;
import com.afra.formutleview.bean.BaseBean;
import com.afra.formutleview.bean.EditTextBean;
import com.afra.formutleview.bean.SpinnerBean;

/**
 * Created by yangshuai on 2018/7/13.
 * {link http://afra55.github.io}
 */

public class SpinnerItemSugar extends ViewSugar{

    public static ViewSugar getInstance(Context context, ViewGroup viewGroup) {
        return new SpinnerItemSugar(context, viewGroup);
    }

    private Spinner spinner;
    private TextView title;

    public SpinnerItemSugar(Context context, ViewGroup parent) {
        super(context, parent);
        title = findView(R.id.title);
        spinner = findView(R.id.planets_spinner);
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_for_spanner;
    }

    @Override
    public <T extends BaseBean> void bind(T obj) {

        if (obj != null && obj instanceof SpinnerBean) {
            final SpinnerBean spinnerBean = (SpinnerBean) obj;
            String[] strings = spinnerBean.getStrings();
            if (strings == null) {
                strings = new String[]{};
            }
            title.setText(spinnerBean.getTitle());

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context,
                    android.R.layout.simple_spinner_item, strings);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(arrayAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    // 像这种点击事件如果错乱，给 view 加 tag，tag 的内容是 adapterPosition 然后在下面判断是否相等
                    String message = (String) adapterView.getItemAtPosition(i);
                    spinnerBean.setSelectString(message);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }

    }
}
