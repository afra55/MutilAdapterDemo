package com.afra.formutleview.sugar;

import android.content.Context;
import android.view.ViewGroup;

import com.afra.formutleview.base.BaseViewHolder;
import com.afra.formutleview.base.ViewSugar;
import com.afra.formutleview.sugar.EditTextItemSugar;
import com.afra.formutleview.sugar.LeftTitleItemSugar;
import com.afra.formutleview.sugar.NineGridItemSugar;
import com.afra.formutleview.sugar.NineGridSugar;
import com.afra.formutleview.sugar.SpinnerItemSugar;

/**
 * Created by yangshuai on 2018/7/13.
 * {link http://afra55.github.io}
 */

public class ViewHolderFactory {

    public static final int NINE_GRID = 901;
    public static final int NINE_GRID_ITEM = 902;
    public static final int LEFT_TITLE_ITEM = 903;
    public static final int EDITTEXT_ITEM = 904;
    public static final int SPINNER_ITEM = 905;

    public static BaseViewHolder getViewHolder(Context context, ViewGroup parent, int type) {
        ViewSugar viewSugar = null;
        switch (type) {
            case NINE_GRID:
                // 在这里获取 ViewSugar 的实现类
                viewSugar = NineGridSugar.getInstance(context, parent);
                break;
            case NINE_GRID_ITEM:
                viewSugar = NineGridItemSugar.getInstance(context, parent);
                break;
            case LEFT_TITLE_ITEM:
                viewSugar = LeftTitleItemSugar.getInstance(context, parent);
                break;
            case EDITTEXT_ITEM:
                viewSugar = EditTextItemSugar.getInstance(context, parent);
                break;
            case SPINNER_ITEM:
                viewSugar = SpinnerItemSugar.getInstance(context, parent);
                break;
            default:
                break;
        }
        if (viewSugar != null) {
            return viewSugar.getViewHolder();
        }
        return null;
    }
}
