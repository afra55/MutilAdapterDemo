package com.afra.formutleview.sugar;

import android.content.Context;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afra.formutleview.R;
import com.afra.formutleview.base.GlideApp;
import com.afra.formutleview.base.ViewSugar;
import com.afra.formutleview.bean.BaseBean;
import com.afra.formutleview.bean.NeneGridItemBean;
import com.afra.formutleview.bean.TitleBean;

/**
 * Created by yangshuai on 2018/7/13.
 * {link http://afra55.github.io}
 */

public class LeftTitleItemSugar extends ViewSugar{

    public static ViewSugar getInstance(Context context, ViewGroup viewGroup) {
        return new LeftTitleItemSugar(context, viewGroup);
    }

    private TextView textView;

    public LeftTitleItemSugar(Context context, ViewGroup parent) {
        super(context, parent);
        textView = findView(R.id.title);
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_for_title;
    }

    @Override
    public <T extends BaseBean> void bind(T obj) {

        if (obj instanceof TitleBean) {
            TitleBean titleBean = (TitleBean) obj;
            textView.setText(titleBean.getTitle());
        }

    }
}
