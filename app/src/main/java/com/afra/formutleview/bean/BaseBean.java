package com.afra.formutleview.bean;

import java.io.Serializable;

/**
 * Created by yangshuai on 2018/7/13.
 * {link http://afra55.github.io}
 */

public class BaseBean implements Serializable {

    private int viewType = -1;

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }
}

