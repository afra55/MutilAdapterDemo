package com.afra.formutleview.bean;

import com.afra.formutleview.sugar.ViewHolderFactory;

/**
 * Created by yangshuai on 2018/7/13.
 * {link http://afra55.github.io}
 */

public class NeneGridItemBean extends BaseBean {

    private String imagePath;

    public NeneGridItemBean(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    @Override
    public int getViewType() {
        return ViewHolderFactory.NINE_GRID_ITEM;
    }
}
