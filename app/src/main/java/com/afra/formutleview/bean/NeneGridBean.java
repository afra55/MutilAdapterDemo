package com.afra.formutleview.bean;

import com.afra.formutleview.sugar.ViewHolderFactory;

import java.util.List;

/**
 * Created by yangshuai on 2018/7/13.
 * {link http://afra55.github.io}
 */

public class NeneGridBean extends BaseBean {

    private List<NeneGridItemBean> list;

    public List<NeneGridItemBean> getList() {
        return list;
    }

    public void setList(List<NeneGridItemBean> list) {
        this.list = list;
    }

    @Override
    public int getViewType() {
        return ViewHolderFactory.NINE_GRID;
    }
}
