package com.afra.formutleview.bean;

import com.afra.formutleview.sugar.ViewHolderFactory;

/**
 * Created by yangshuai on 2018/7/14.
 * {link http://afra55.github.io}
 */

public class TitleBean extends BaseBean{
    private String title;
    private int type;

    public TitleBean(String title, int type) {
        this.title = title;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {

        this.type = type;
    }

    @Override
    public int getViewType() {
        return ViewHolderFactory.LEFT_TITLE_ITEM;
    }
}
