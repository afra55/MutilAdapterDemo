package com.afra.formutleview.bean;

import com.afra.formutleview.sugar.ViewHolderFactory;

/**
 * Created by yangshuai on 2018/7/14.
 * {link http://afra55.github.io}
 */

public class EditTextBean extends BaseBean {
    private String title;
    private String content;

    public EditTextBean(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public int getViewType() {
        return ViewHolderFactory.EDITTEXT_ITEM;
    }
}
