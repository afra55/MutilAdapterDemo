package com.afra.formutleview.bean;

import com.afra.formutleview.sugar.ViewHolderFactory;

/**
 * Created by yangshuai on 2018/7/14.
 * {link http://afra55.github.io}
 */

public class SpinnerBean extends BaseBean {
    private String title;
    private String[] strings;
    private String selectString;

    public SpinnerBean(String title, String[] strings) {
        this.title = title;
        this.strings = strings;
    }

    public String getTitle() {
        return title;
    }

    public String getSelectString() {
        return selectString;
    }

    public void setSelectString(String selectString) {
        this.selectString = selectString;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String[] getStrings() {
        return strings;
    }

    public void setStrings(String[] strings) {
        this.strings = strings;
    }

    @Override
    public int getViewType() {
        return ViewHolderFactory.SPINNER_ITEM;
    }
}
