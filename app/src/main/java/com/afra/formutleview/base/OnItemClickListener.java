package com.afra.formutleview.base;

/**
 * Created by yangshuai on 2018/7/13.
 * {link http://afra55.github.io}
 */

public interface OnItemClickListener {
    void onItemClicked(int position, Object obj);

    boolean onItemLongClicked(int position, Object obj);
}
