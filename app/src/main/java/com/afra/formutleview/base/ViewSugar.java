package com.afra.formutleview.base;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.view.View;
import android.view.ViewGroup;

import com.afra.formutleview.bean.BaseBean;

/**
 * Created by yangshuai on 2018/7/13.
 * {link http://afra55.github.io}
 */

public abstract class ViewSugar {
    protected Context context;
    protected ViewGroup parent;
    protected BaseViewHolder baseViewHolder;
    protected View rootView;

    public ViewSugar(Context context, ViewGroup parent) {
        this.context = context;
        this.parent = parent;
        baseViewHolder = createViewHolder(getLayoutId());
        rootView = baseViewHolder.getItemView();
    }

    public abstract @LayoutRes
    int getLayoutId();


    public abstract <T extends BaseBean> void bind(T obj);

    private BaseViewHolder createViewHolder(int layoutId) {
        return BaseViewHolder.createViewHolder(context, parent, layoutId);
    }

    public BaseViewHolder getViewHolder() {
        baseViewHolder.setTag(this);
        return baseViewHolder;
    }

    protected <T extends View> T  findView(int viewId) {
        return rootView.findViewById(viewId);
    }
}
