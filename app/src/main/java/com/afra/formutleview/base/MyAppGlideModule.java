package com.afra.formutleview.base;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by yangshuai on 2018/7/13.
 * {link http://afra55.github.io}
 */

@GlideModule
public final class MyAppGlideModule extends AppGlideModule {}
