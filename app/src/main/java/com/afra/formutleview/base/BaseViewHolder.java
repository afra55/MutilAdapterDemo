package com.afra.formutleview.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afra.formutleview.R;

/**
 * Created by yangshuai on 2018/7/13.
 * {link http://afra55.github.io}
 */

public class BaseViewHolder extends RecyclerView.ViewHolder {

    private View itemView;
    protected Context context;

    public BaseViewHolder(Context context, View itemView) {
        super(itemView);
        this.itemView = itemView;
        this.context = context;
    }
    public View getItemView() {
        return itemView;
    }

    public static BaseViewHolder createViewHolder(Context context, View itemView) {
        return new BaseViewHolder(context, itemView);
    }

    public static BaseViewHolder createViewHolder(Context context,
                                                  ViewGroup parent, int layoutId) {
        View itemView = LayoutInflater.from(context).inflate(layoutId, parent,
                false);
        return new BaseViewHolder(context, itemView);
    }

    public void setTag(ViewSugar viewItem) {
        // 设置 tag 文件，不能直接设置 tag，需要设置相应的 key，才不会造成冲突
        itemView.setTag(R.integer.sugar, viewItem);

    }

    public ViewSugar getTag() {
        Object tag = itemView.getTag(R.integer.sugar);
        if (tag != null
                && tag instanceof ViewSugar) {
            return (ViewSugar) tag;
        }
        return null;
    }


}
