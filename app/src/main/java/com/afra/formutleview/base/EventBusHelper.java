package com.afra.formutleview.base;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by yangshuai on 2018/7/14.
 * {link http://afra55.github.io}
 */

public class EventBusHelper {
    public static void post(String type, Object object, Object extra) {
        EventBus.getDefault().post(new MessageEvent(type, object, extra));
    }

    public static void post(String type, Object object) {
        EventBus.getDefault().post(new MessageEvent(type, object));
    }

    public static void postSticky(String type, Object object) {
        EventBus.getDefault().postSticky(new MessageEvent(type, object));
    }

    public static void postSticky(String type, Object object, Object extra) {
        EventBus.getDefault().postSticky(new MessageEvent(type, object, extra));
    }


    public static void register(Object object) {
        EventBus.getDefault().register(object);
    }

    public static void unRegister(Object object) {
        if (EventBus.getDefault().isRegistered(object)) {
            EventBus.getDefault().unregister(object);
        }
    }

}
