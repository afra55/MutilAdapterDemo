package com.afra.formutleview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.afra.formutleview.base.BaseRecyclerAdapter;
import com.afra.formutleview.base.EventBusHelper;
import com.afra.formutleview.base.MessageEvent;
import com.afra.formutleview.base.OnItemClickListener;
import com.afra.formutleview.bean.BaseBean;
import com.afra.formutleview.bean.EditTextBean;
import com.afra.formutleview.bean.NeneGridBean;
import com.afra.formutleview.bean.NeneGridItemBean;
import com.afra.formutleview.bean.SpinnerBean;
import com.afra.formutleview.bean.TitleBean;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class MainActivity<T extends BaseBean> extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView titleRecyclerView;

    private BaseRecyclerAdapter rightAdapter;
    private BaseRecyclerAdapter leftAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recycler_view);
        titleRecyclerView = findViewById(R.id.title_recycler_view);
        LinearLayoutManager layout = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layout);
        titleRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        EventBusHelper.register(this);


        leftAdapter = new BaseRecyclerAdapter(this);
        titleRecyclerView.setAdapter(leftAdapter);

        initLeftAdatperData();

        rightAdapter = new BaseRecyclerAdapter<>(this);
        recyclerView.setAdapter(rightAdapter);
        initRightAdapter(1);
    }

    private void initLeftAdatperData() {
        List<TitleBean> leftTitleBeanList = new ArrayList<>();

        leftTitleBeanList.add(new TitleBean("1", 1));
        leftTitleBeanList.add(new TitleBean("2", 2));
        leftTitleBeanList.add(new TitleBean("3", 3));

        leftAdapter.init(leftTitleBeanList);
        leftAdapter.notifyDataSetChanged();

        leftAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClicked(int position, Object obj) {
                if (obj instanceof TitleBean) {
                    // 点击左边title 变右边布局
                    TitleBean titleBean = (TitleBean) obj;
                    initRightAdapter(titleBean.getType());
                }
            }

            @Override
            public boolean onItemLongClicked(int position, Object obj) {
                return false;
            }
        });
    }

    private void initRightAdapter(int type) {

        List<Object> rightList = new ArrayList<>();

        switch (type) {
            case 1:
                addEditText(rightList, "名字", "哈哈");

                addNineGridItem(rightList);
                addNineGridItem(rightList);
                addNineGridItem(rightList);
                addNineGridItem(rightList);

                rightList.add(new SpinnerBean("Spinner", new String[]{"One", "Two", "Three"}));

                addEditText(rightList, "性别", "");

                rightList.add(new SpinnerBean("Spinner2", new String[]{"One2", "Two2", "Three2"}));
                break;
            case 2:
                rightList.add(new SpinnerBean("Spinner", new String[]{"One", "Two", "Three"}));

                addNineGridItem(rightList);
                addEditText(rightList, "名字", "哈哈");
                addNineGridItem(rightList);
                addNineGridItem(rightList);
                addNineGridItem(rightList);


                rightList.add(new SpinnerBean("Spinner2", new String[]{"One2", "Two2", "Three2"}));
                addEditText(rightList, "性别", "");

                break;
            case 3:
                addNineGridItem(rightList);
                addEditText(rightList, "名字", "哈哈");
                addEditText(rightList, "性别", "");

                addNineGridItem(rightList);
                addNineGridItem(rightList);
                addNineGridItem(rightList);


                rightList.add(new SpinnerBean("Spinner", new String[]{"One", "Two", "Three"}));

                rightList.add(new SpinnerBean("Spinner2", new String[]{"One2", "Two2", "Three2"}));
                break;
            default:
                break;
        }

        addEditText(rightList, "名字", "哈哈");

        addNineGridItem(rightList);
        addNineGridItem(rightList);
        addNineGridItem(rightList);
        addNineGridItem(rightList);

        rightList.add(new SpinnerBean("Spinner", new String[]{"One", "Two", "Three"}));

        addEditText(rightList, "性别", "");

        rightList.add(new SpinnerBean("Spinner2", new String[]{"One2", "Two2", "Three2"}));

        rightAdapter.init(rightList);
        rightAdapter.notifyDataSetChanged();
    }

    private void addEditText(List<Object> rightList, String title, String content) {
        rightList.add(new EditTextBean(title, content));
    }

    private void addNineGridItem(List<Object> rightList) {
        // 一个九宫格布局
        NeneGridBean neneGridBean = new NeneGridBean();

        // 九宫格图片 item
        List<NeneGridItemBean> itemBeanList = new ArrayList<>();
        itemBeanList.add(new NeneGridItemBean("http://pic1.win4000.com/wallpaper/2018-07-11/5b45a8e214433_270_185.jpg"));
        itemBeanList.add(new NeneGridItemBean("http://pic1.win4000.com/wallpaper/2018-07-11/5b45a8e214433_270_185.jpg"));
        itemBeanList.add(new NeneGridItemBean("http://pic1.win4000.com/wallpaper/2018-07-11/5b45a8e214433_270_185.jpg"));
        itemBeanList.add(new NeneGridItemBean("http://pic1.win4000.com/wallpaper/2018-07-11/5b45a8e214433_270_185.jpg"));
        neneGridBean.setList(itemBeanList);

        rightList.add(neneGridBean);
    }

    @Override
    protected void onDestroy() {
        EventBusHelper.unRegister(this);
        super.onDestroy();

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {

        String type = event.getType();
        Object object = event.getObject();
        switch (type) {
            case "添加图片":
                if (object != null && object instanceof Integer) {
                    int index = (int) object;
                    BaseBean item = rightAdapter.getItem(index);
                    if (item instanceof NeneGridBean) {
                        NeneGridBean gridBean = (NeneGridBean) item;
                        List<NeneGridItemBean> list = gridBean.getList();
                        if (list == null) {
                            list = new ArrayList<>();
                        }
                        if (!list.isEmpty()) {
                            // 判断最后一张是不是加号
                            NeneGridItemBean itemBean = list.get(list.size() - 1);
                            if (TextUtils.isEmpty(itemBean.getImagePath())) {
                                list.remove(itemBean);
                            }
                        }
                        // 添加图片
                        list.add(new NeneGridItemBean("http://pic1.win4000.com/wallpaper/2018-07-11/5b45a8e214433_270_185.jpg"));
                        gridBean.setList(list);
                        rightAdapter.notifyItemChanged(index);
                    }
                }
                break;
            default:

                break;
        }
    }

}
